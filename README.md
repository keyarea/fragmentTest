# 碎片 Fragment

> 碎片(Fragment)是一种可以嵌入到活动中的UI片段，它能够让程序更加合理和充分的利用大屏幕空间。

## 碎片的使用方式

### 碎片的简单用法

> 在一个活动当中添加两个碎片，并让这两个碎片平分活动空间。

新建两个碎片布局：

```xml
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    android:orientation="vertical"
    android:layout_width="match_parent"
    android:layout_height="match_parent">

    <Button
        android:id="@+id/button"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:layout_gravity="center_horizontal"
        android:text="Button"
        />

</LinearLayout>
```

```xml
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    android:orientation="vertical"
    android:background="#00ff00"
    android:layout_width="match_parent"
    android:layout_height="match_parent">

    <TextView
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:layout_gravity="center_horizontal"
        android:textSize="20sp"
        android:text="This is right fragment"
        />

</LinearLayout>
```

接下来，编写Fragment的代码:

```java
public class LeftFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
       View view = inflater.inflate(R.layout.left_fragment, container, false);
       return view;
    }
}
```

```java
public class RightFragment extends Fragment {
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
       View view = inflater.inflate(R.layout.right_fragment, container, false);
       return view;
    }
}
```

接下来，修改活动的布局文件

```xml
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    android:layout_height="match_parent"
    android:layout_width="match_parent"
    android:orientation="horizontal"
    >
    <fragment
        android:id="@+id/left_fragment"
        android:name="net.qipo.fragmenttest.LeftFragment"
        android:layout_height="match_parent"
        android:layout_width="0dp"
        android:layout_weight="1"
    />
    
    <fragment
        android:id="@+id/right_fragment"
        android:name="net.qipo.fragmenttest.RightFragment"
        android:layout_width="0dp"
        android:layout_height="match_parent"
        android:layout_weight="1"/>
    
</LinearLayout>
```

这样最简单的碎片示例已经写好了，效果如下图所示:

![](./images/fragment.jpg)

### 动态添加碎片

新建另一个碎片的布局文件`another_right_fragment.xml`:

```xml
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    android:orientation="vertical"
    android:background="#ffff00"
    android:layout_width="match_parent"
    android:layout_height="match_parent">

    <TextView
        android:textSize="20sp"
        android:layout_gravity="center_horizontal"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:text="This is another right fragment"
        />

</LinearLayout>
```

新建`AnotherRightFragment`作为另一个右侧碎片:

```java
public class AnotherRightFragment extends Fragment {
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.another_right_fragment, container, false);
        return  view;
    }
}
```

修改活动的布局文件，以实现动态添加碎片

```xml
<?xml version="1.0" encoding="utf-8"?>
<LinearLayout xmlns:android="http://schemas.android.com/apk/res/android"
    android:layout_height="match_parent"
    android:layout_width="match_parent"
    android:orientation="horizontal"
    >
    <fragment
        android:id="@+id/left_fragment"
        android:name="net.qipo.fragmenttest.LeftFragment"
        android:layout_height="match_parent"
        android:layout_width="0dp"
        android:layout_weight="1"
    />
    
    <FrameLayout
        android:id="@+id/right_layout"
        android:layout_width="0dp"
        android:layout_height="match_parent"
        android:layout_weight="1"/>
    
</LinearLayout>
```

接下来，修改活动的代码，以实现动态添加碎片的功能:

```java
public class StartActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
        Button button = findViewById(R.id.button);
        button.setOnClickListener(this);
        replaceFragment(new RightFragment());
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button:
                replaceFragment(new AnotherRightFragment());
                break;
            default:
                break;
        }
    }

    private void replaceFragment(Fragment fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.right_layout, fragment);
        transaction.commit();
    }
}
```

> 动态添加碎片主要分为5步:
1. 创建待添加的碎片实例；
2. 获取`FragmentManager`，在活动中可以直接通过调用`getSupportFragmentManager()`方法获取；
3. 开启一个事务，通过调用`beginTransaction()`方法开始;
4. 向容器中添加或者替换碎片，一般使用`replace()`方法实现，需要传入容器的id和待添加的碎片实例;
5. 提交事务，调用`commit()`方法来完成。

![](./images/fragment1.gif)

### 在碎片中模拟返回栈

在前面的示例中我们会发现，按一下back键就会直接退出了。如果我们想要类似于返回栈的效果，按下back键可以返回上一个碎片，如何实现呢?

`FragmentTransaction`提供了一个`addToBackStack()`方法，可以用于将一个事务添加到返回栈中，如下所示:

```java
    private void replaceFragment(Fragment fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.right_layout, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }
```

它接收一个名字用于描述返回栈的状态，一般传入null既可。这样就会实现该效果了。

### 碎片与活动之间进行通信

碎片与活动都各自存在于一个独立的类中，它们之间并没有那么明显的方式来直接进行通信。但是可以在活动中调用碎片的方法，或者在碎片中调用活动的方法。


在活动中我们可以通过`FragmentManager`获取到碎片的实例:

```java
RightFrament rightFragment = getSupportFragmentManager().findFragmentById(R.id.right_fragment);
```

在碎片中获取活动的实例:

```java
MainActivity mainActivity = getActivity();
```

有了实例之后，在实例中调用方法就变得轻而易举了。那么就可以使用这种方法进行通信了。

## 活动的生命周期

### 碎片的状态和回调

- 运行状态

> 当一个碎片是可见的，并且它关联的活动正处于运行状态时，该碎片也处于运行状态。

- 暂停状态

> 当一个活动进入暂停状态时（由于另一个未占满屏幕的活动被添加到了栈顶），与它相关联的可见碎片就会进入到暂停状态。

- 停止状态

> 当一个活动进入停止状态时，与它相关联的碎片碎片就会进入到停止状态，或者通过调用`FragmentTransaction`的`remove()`.`replace()`方法将碎片从活动中移除，但如果在事务提交之前调用`addToBackStack()`方法，这时的碎片也会进入到停止状态。总的来说，进入停止状态的碎片对用户来说是完全不可见的，有可能会被系统回收。

- 销毁状态

> 碎片总是依附于活动而存在，因此当活动被销毁时，与它相关联的碎片就会进入到销毁状态。或者通过调用`FragmentTransaction`的`remove()`,`replace()`方法将碎片从活动中移除，但在事物提交之前并没有调用`addToBackStack()`方法，这时的碎片也会进入到销毁状态。

碎片也提供了一些附加的回调方法:

- `onAttach()`:当碎片和活动建立关联的时候调用。
- `onCreateView()`: 为碎片创建视图（加载布局）时调用。
- `onActivityCreated()`: 确保与碎片相关联的活动一定已经创建完毕的时候调用。
- `onDestroyView()`: 当与碎片关联的视图被移除的时候调用。
- `onDetach()`: 当碎片和活动解除关联的时候调用。

![](./images/lifecycle.jpg)

## 动态加载布局的技巧

### 使用限定符

Android中一些常见的限定符参考下表:

|屏幕特征|限定符|描述|
|:--:|:--:|:--:|
|大小|small|提供给小屏幕设备的资源|
|大小|normal|提供给中等屏幕设备的资源|
|大小|large|提供给大屏幕设备的资源|
|大小|xlarge|提供给超大屏幕设备的资源|
|分辨率|ldpi|提供给低分辨率设备的资源（120dpi以下）|
|分辨率|mdpi|提供给中等分辨率设备的资源（120-160dpi）|
|分辨率|hdpi|提供给高分辨率设备的资源(160dpi-240dpi)|
|分辨率|xhdpi|提供给超高分辨率设备的资源(240dpi-320dpi)|
|分辨率|xxhdpi|提供给超超高分辨率设备的资源（320dpi-480dpi）|
|方向|land|提供给横屏设备的资源|
|方向|port|提供给竖屏设备的资源|

在res中新建`layout-large`文件夹，其中`large`就是限定符，那些屏幕被认为是`large`的设备就会自动加载`layout-large`文件夹下的布局，而小屏幕的设备则还是会加载`layout`文件夹下的布局。

### 使用最小宽度限定符

有时候我们希望可以更加灵活地为不同设备加载布局，不管它们是不是被系统认定为`large`,这时可以使用宽度限定符。

> 最小宽度限定符允许我们对屏幕的宽度指定一个最小值（以dp为单位），然后以这个最小值为临界点，屏幕宽度大于这个值就加载一个布局，屏幕宽度小于这个值的设备就加载另一个布局。

如在res目录下新建`layout-sw600dp`文件夹，当程序运行在屏幕宽度大于600dp的设备上时，就会加载这个文件夹下的布局，当程序运行在屏幕宽度小于600dp的设备上时，则仍然加载默认的`layout`文件夹下的布局。


























